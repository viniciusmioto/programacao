#include <stdio.h>
#include <stdlib.h>

#define AGENDA_SIZE 3
#define DESC_SIZE 10

struct Date
{
    int day;
    int month;
    int year;
};

struct Moment
{
    int hour;
    int minutes;
};

struct Appointment 
{
    char *name;
    struct Date date;
    struct Moment moment;
};

struct Agenda
{
    int scheduled;
    struct Appointment agenda[8];
};

int make_appointment(struct Agenda *my_agenda, struct Appointment *appointment, int scheduled)
{
    appointment = malloc(sizeof(struct Appointment));
    appointment->name = calloc(DESC_SIZE, sizeof(char));
    printf("Enter the day (1-30): ");
    scanf("%d", &appointment->date.day);
    printf("Enter the month (1-12): ");
    scanf("%d", &appointment->date.month);
    printf("Enter the hour (0-23): ");
    scanf("%d", &appointment->moment.hour);
    printf("Enter the minute (0-59): ");
    scanf("%d", &appointment->moment.minutes);
    getchar();
    printf("Enter the description to the appointment: ");
    fgets(appointment->name, DESC_SIZE - 1, stdin);
    my_agenda->agenda[scheduled] = *appointment;
    free(appointment->name);
    free(appointment);
    return ++scheduled;
}

void show_agenda(struct Agenda my_agenda)
{
    int i;
    printf("-- MINHA AGENDA --\n");
    for (i = 0; i < my_agenda.scheduled; i++)
    {
        printf("%s", my_agenda.agenda[i].name);
        printf("%d/%d - ", my_agenda.agenda[i].date.day, my_agenda.agenda[i].date.month);
        printf("%d:%d\n", my_agenda.agenda[i].moment.hour, my_agenda.agenda[i].moment.minutes);
    }
}

void init_agenda(struct Agenda *my_agenda)
{
    my_agenda->scheduled = 0;
}

int main()
{
    struct Agenda my_agenda;
    init_agenda(&my_agenda);
    while (my_agenda.scheduled < AGENDA_SIZE)
        my_agenda.scheduled=make_appointment(&my_agenda, &my_agenda.agenda[my_agenda.scheduled], my_agenda.scheduled);
    show_agenda(my_agenda);
    return 0;
}
