#include "stack.h"
#include "queue.h"
#include "prog_lib.h"

typedef struct Professional
{
    char name[6];
    int employment;
    int working_time;
    int had_break;
    int first_dose_counter[2];
    int second_dose_counter[2];
} Professional;

int get_random(int min, int max);

int get_priority(int probability);

void create_names_stack(Stack *names_stack);

void get_name(Professional *p, Stack *names_stack);

void incoming_patients(Queue *queue);

int call_next(Professional *professional, Queue *queue, int priority_done[2], int dose);

int call_last(Professional *professional, Queue *queue, int priority_done[2], int dose);

void close_place(int first_dose_done[2], int second_dose_done[2], Queue first_dose_queue, Queue second_dose_queue, Professional professionals[5], int quantity);

void init_professionals(Professional professionals[5], Stack *names_stack, int quantity);

void show_professionals(Professional professionals[5], int quantity);

void show_work(Professional professional, Patient current_patient);