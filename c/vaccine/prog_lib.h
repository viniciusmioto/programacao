/* Prioridade do Paciente */
#define HIGH_PRIORITY 20
#define NORMAL_PRIORITY 10

/* Expedientes */
#define PART_TIME 4 
#define INTERN 6 
#define FULL_TIME 8
#define OVERTIME 10 

int get_random(int min, int max);

void copy_strings(char string_1[6], char string_2[6], int size);

void generate_ticket(char ticket[5]);

int get_priority(int probability);