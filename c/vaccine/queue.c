#include <stdio.h>
#include <stdlib.h>
#include "prog_lib.h"

typedef struct Patient
{
    int priority;
	char ticket[5];
    struct Patient *next, *prev;
} Patient;

typedef struct Queue
{
    Patient *tail, *head;  
} Queue;

/* Cria uma fila vazia, isto é, head e tail apontam para NULL; */
void init_queue(Queue *queue)
{
    queue->head = NULL;
    queue->tail = NULL;
}

/* Retorna 1 se a fila está vazia e zero caso contrário; */
int empty_queue(Queue queue)
{
    return queue.head == NULL;
}

/* Imprime os elementos da fila, do início ao final. Se ela for vazia não imprime nada */
void print_queue(Queue queue)
{
	Patient *curr;

	for (curr = queue.head; curr != NULL; curr = curr->next)
		printf("#%d |%s| ", curr->priority, curr->ticket);

	printf("\n\n");
}

/* Remove todos os elementos da fila e faz com que head e tail apontem para NULL; */
void deallocate_queue(Queue *queue)
{
	Patient *curr = queue->head;

	if (queue->head == NULL)
		return;

	while (curr->next != NULL)	
	{
		curr = curr->next;
		free(curr->prev);
	}

	free(curr);
	queue->tail = NULL;
	queue->head = NULL;
}

/* Insere o elemento p no início da fila; Retorna 1 se a operação foi bem sucedida e zero caso contrário; */
int insert_beggining(Queue *queue, int p, char ticket[5])
{
	Patient *new_patient = malloc(sizeof(Patient));

	if (new_patient == NULL)
		return 0;

	new_patient->priority = p;
	new_patient->prev = NULL;
	copy_strings(new_patient->ticket, ticket, 5);

	if (!empty_queue(*queue)) 
    {
        new_patient->next = queue->head;
        (queue->head)->prev = new_patient;
        queue->head = new_patient;
    }
    else
    {
	    new_patient->next = NULL;
	    queue->head = new_patient;
	    queue->tail = new_patient;
    }
  
    return 1;
}

/* Insere o elemento p no final da fila; Retorna 1 se a operação foi bem sucedida e zero caso contrário; */
int insert_end(Queue *queue, int p, char ticket[5])
{
	Patient *new_patient = malloc(sizeof(Patient));

	if (new_patient == NULL)
		return 0;

	if (empty_queue(*queue))
	{
		free(new_patient);
		return insert_beggining(queue, p, ticket);
	}

	new_patient->priority = p;
	copy_strings(new_patient->ticket, ticket, 5);
	new_patient->next = NULL;
	new_patient->prev = queue->tail;
	(queue->tail)->next = new_patient;
	queue->tail = new_patient;
    return 1;
}

/* Recebe uma fila, uma prioridade (p) e uma senha 
 * insere o elemento na fila, respeitando as prioridades */
int enqueue(Queue *queue, int p, char ticket[5])
{
	Patient *curr;
	Patient *new_patient = malloc(sizeof(Patient));

	/* caso esse seja o primeiro elemento da fila */
	if (empty_queue(*queue) || p > queue->head->priority)
	{
		free(new_patient);
		return insert_beggining(queue, p, ticket);
	}

	/* caso seja o último da fila  */
	if (p <= queue->tail->priority) 
	{
		free(new_patient);
		return insert_end(queue, p, ticket);
	}
	
	if (new_patient == NULL)
		return 0;	

	for (curr = queue->head; curr->priority >= p; curr = curr->next);

	new_patient->priority = p;
	copy_strings(new_patient->ticket, ticket, 5);
	new_patient->prev = curr->prev;
	new_patient->next = curr;
	curr->prev = new_patient;
	(new_patient->prev)->next = new_patient;

    return 1;
}

/* Recebe uma fila 
 * preenche a fila com um número de 50 pacientes antes do atendimento começar */
void queue_up(Queue *queue)
{
    int i, priority;
    char ticket[5];

    for (i = 0; i < 50; i++)
    {
        generate_ticket(ticket);
        priority = get_priority(70);
        enqueue(queue, priority, ticket);
    }

    print_queue(*queue);
}

/* Remove o primeiro elemento da fila. Retorna 1 se a operação foi bem sucedida e zero caso contrário; */
int remove_first(Queue *queue)
{
	if (empty_queue(*queue))
		return 0;

	Patient *curr = queue->head;

	if (queue->head->next == NULL)
	{
		deallocate_queue(queue);
		return 1;
	}
	
	queue->head = queue->head->next;
	queue->head->prev = NULL;
	curr->prev = NULL;
	curr->next = NULL;	
	free(curr);	
	return 1;
}

/* Remove o último elemento da fila. Retorna 1 se a operação foi bem sucedida e zero caso contrário; */
int remove_last_patient(Queue *queue, Patient *patient)
{
	if (empty_queue(*queue))
		return 0;

	if (patient->prev != NULL)
		patient->prev->next = patient->next;

	if (patient->next != NULL)
		patient->next->prev = patient->prev;

	queue->tail = patient->prev;
	free(patient);
	return 1;
}