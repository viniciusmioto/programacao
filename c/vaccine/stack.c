#include <stdio.h>
#include <stdlib.h>

typedef struct Node
{
	char name[5];
    struct Node *next, *prev;
} Node;

typedef struct Stack
{
    Node *tail, *head;  
} Stack;

void set_name(Node *node, char name[6])
{
	int i;
	for (i = 0; i < 5; i++)
		node->name[i] = name[i];
	
	node->name[5] = '\0';
}

/* Cria uma pilha vazia, isto é, head e tail apontam para NULL; */
void init_stack(Stack *stk)
{
    stk->head = NULL;
    stk->tail = NULL;
}

/* Retorna 1 se a pilha está vazia e zero caso contrário; */
int empty_stack(Stack stk)
{
    return stk.head == NULL;
}

/* Imprime os elementos da pilha, do início ao final. Se ela for vazia não imprime nada */
void print_stack(Stack stk)
{
	Node *curr;

	for (curr = stk.head; curr != NULL; curr = curr->next)
		printf(" %s ", curr->name);

	printf("\n");
}

/* Remove todos os elementos da pilha e faz com que head e tail apontem para NULL; */
void deallocate_stack(Stack *stk)
{
	Node *curr = stk->head;

	if (stk->head == NULL)
		return;

	while (curr->next != NULL)	
	{
		curr = curr->next;
		free(curr->prev);
	}

	free(curr);
	stk->tail = NULL;
	stk->head = NULL;
}

/* Insere o elemento p no início da pilha; Retorna 1 se a operação foi bem sucedida e zero caso contrário; */
int push(Stack *stk, char name[5])
{
	Node *new_node = malloc(sizeof(Node));

	if (new_node == NULL)
		return 0;

	new_node->prev = NULL;
	set_name(new_node, name);

	if (!empty_stack(*stk)) 
    {
        new_node->next = stk->head;
        (stk->head)->prev = new_node;
        stk->head = new_node;
    }
    else
    {
	    new_node->next = NULL;
	    stk->head = new_node;
	    stk->tail = new_node;
    }
  
    return 1;
}

/* Remove o primeiro elemento da pilha. Retorna 1 se a operação foi bem sucedida e zero caso contrário; */
int pop(Stack *stk)
{
	Node *curr = stk->head;

	if (empty_stack(*stk))
		return 0;

	if (stk->head->next == NULL)
	{
		deallocate_stack(stk);
		return 1;
	}
	
	stk->head = stk->head->next;
	stk->head->prev = NULL;
	curr->prev = NULL;
	curr->next = NULL;	
	free(curr);	
	return 1;
}