#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "stack.h"
#include "queue.h"
#include "prog_lib.h"

typedef struct Professional
{
    char name[6];
    int employment;
    int working_time;
    int had_break;
    int first_dose_counter[2];
    int second_dose_counter[2];
} Professional;

/* Insere um núm aleatório de pacientes na fila 
 * Ideia legal mas não utilizada (fila dinâmica)
void incoming_patients(Queue *queue)
{
    int priority, i;
    char ticket[5];

    for (i = 0; i < get_random(0, 2); i++)
    {
        priority = get_priority(70);
        generate_ticket(ticket);
        enqueue(queue, priority, ticket);
    }
}

*/

/* Preenche uma pilha com 5 nomes */
void create_names_stack(Stack *names_stack)
{
    int i;
    char names[5][6] = {"SMITH", "JONES", "BROWN", "DAVIS", "OWENS"};
    
    for (i = 0; i < 5; i++)
        push(names_stack, names[i]);
}

/* Recebe um Profssional e Paciente
   Imprime quem está atendendo, o tempo de trab desse profissional e quem está sendo atendido */
void show_work(Professional professional, Patient current_patient)
{
    printf("Profissional: %s | Tempo de Trabalho: %d min | Atendendo ao paciente com a senha: #%d |%s|\n\n", professional.name, professional.working_time, current_patient.priority, current_patient.ticket);
}

/* Recebe um profissional, uma fila, um vetor de contagem de prioridade e qual dose está sendo aplicada
 * Atende o primeiro paciente da fila e atualiza os contadores */
int call_next(Professional *professional, Queue *queue, int priority_done[2], int dose)
{
    if (empty_queue(*queue))
        return 0;

    priority_done[(queue->head->priority)/10 - 1]++;
    if (dose == 1)
    {
        professional->first_dose_counter[(queue->head->priority)/10 - 1]++;
    }
    else
    {
        professional->second_dose_counter[(queue->head->priority)/10 - 1]++;
    }
    professional->working_time += (queue->head->priority);
    show_work(*professional, *queue->head);
    remove_first(queue);
    /* incoming_patients(queue); */
    return 1;
}

/* Recebe um profissional, uma fila, um vetor de contagem de prioridade e qual dose está sendo aplicada
 * Atende o últomo paciente da fila e atualiza os contadores (Starvation Bônus)  */
int call_last(Professional *professional, Queue *queue, int priority_done[2], int dose)
{
    if (empty_queue(*queue))
        return 0;

    priority_done[(queue->tail->priority)/10 - 1]++;
    if (dose == 1)
    {
        professional->first_dose_counter[(queue->tail->priority)/10 - 1]++;
    }
    else
    {
        professional->second_dose_counter[(queue->tail->priority)/10 - 1]++;
    }
    professional->working_time += (queue->tail->priority);
    printf(" ~ STARVATION ~  ");
    show_work(*professional, *queue->tail);
    remove_last_patient(queue, queue->tail);
    /* incoming_patients(queue); */
    return 1;
}

/* Recebe um vetor estático de Profissionais e um número inteiro entre 1 e 5 
   Preenche as informações da quantidade de profissionais informada */
void init_professionals(Professional professionals[5], Stack *names_stack, int quantity)
{
    int i, j;
    init_stack(names_stack);
    create_names_stack(names_stack);

    for (i = 0; i < quantity; i++)
    {
        copy_strings(professionals[i].name, names_stack->head->name, 5);
        pop(names_stack);
        professionals[i].employment = get_random(2, 5) * 2;
        professionals[i].working_time = 0;
        professionals[i].had_break = 0;
        for (j = 0; j <2; j++)
        {
            professionals[i].first_dose_counter[j] = 0;
            professionals[i].second_dose_counter[j] = 0;
        }
        if (professionals[i].employment == PART_TIME)
        {
            professionals[i].had_break = 1;
        }
    }
}

/* Recebe um vetor de Profissionais e uma quantidade (inteiro)
 * Imprime as principais informações sobre esses Profissionais */
void show_professionals(Professional professionals[5], int quantity)
{
    int i, j;

    char employment_type[4][7] = {"PART_T\0", "INTERN\0", "FULL_T\0", "EXTRA_\0"};

    printf("\n------------PROFISSIONAIS-------------\n");
    printf("Quantidade de Profissionais: %d\n", quantity);
    for (i = 0; i < quantity; i++)
    {
        printf("> Profissional: %s\n", professionals[i].name);
        printf(" - Tipo de Expediente: %s\n", employment_type[professionals[i].employment/2 -2]);
        printf(" - Quantidade de Minutos Trabalhados: %d\n", professionals[i].working_time);
        for (j = 0; j <2; j++)
            printf("   * Atendeu %d pessoas da 1ª Dose de Prioridade #%d0 \n", professionals[i].first_dose_counter[j], j+1);
        for (j = 0; j <2; j++)
            printf("   * Atendeu %d pessoas da 2ª Dose de Prioridade #%d0 \n", professionals[i].second_dose_counter[j], j+1);
        
        printf("\n");
    }
    printf("-------------------------------------\n");
}

/* Imprime quantos pacientes de cada prioridade foram atendidos */
void close_place(int first_dose_done[2], int second_dose_done[2], Queue first_dose_queue, Queue second_dose_queue, Professional professionals[5], int quantity)
{
    int i;

    printf("\n------------VACINADOS-------------\n");
    printf(" ## Vacina UFPR 1ª Dose:\n");
    for (i = 0; i < 2; i++)
        printf(" * Prioridade #%d0: %d\n", (i + 1), first_dose_done[i]);

    printf(" ## Vacina UFPR 2ª Dose:\n");
    for (i = 0; i < 2; i++)
        printf(" * Prioridade #%d0: %d\n", (i + 1), second_dose_done[i]);


    printf("\n------------PACIENTES NÃO ATENDIDOS-------------\n");
    if (empty_queue(first_dose_queue))
    {
        printf("Todos da Primeira Dose foram atendidos\n");
    }
    else
    {
        printf("Pacientes não atendidos da Primeira Dose:\n");
        print_queue(first_dose_queue);
    }

    if (empty_queue(second_dose_queue))
    {
        printf("Todos da Segunda Dose foram atendidos\n");
    }
    else
    {
        printf("Pacientes não atendidos da Segunda Dose:\n");
        print_queue(second_dose_queue);
    }
    
    show_professionals(professionals, quantity);
}