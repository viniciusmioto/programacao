#include <stdlib.h>

/* Prioridade do Paciente */
#define HIGH_PRIORITY 20
#define NORMAL_PRIORITY 10

/* Expedientes */
#define PART_TIME 4 
#define INTERN 6 
#define FULL_TIME 8
#define OVERTIME 10 

/* Gera um número aleatório no intervalo [min..max] */
int get_random(int min, int max)
{
    return min + (rand() % (max - min + 1));
}

/* Recebe duas strings e um tamanho. Copia os caracteres da string 2 para a string 1 */
void copy_strings(char string_1[6], char string_2[6], int size)
{
    int i;

    for (i = 0; i < size; i++)
        string_1[i] = string_2[i];

    string_1[size] = '\0';
}

/* Recebe uma probabilidade. 
 * Gera uma prioridade de forma aleatória, com probabilidade P% para alta prioridade e (100-P)% para não prioritário */
int get_priority(int probability)
{
    if (get_random(1, 100) <= probability)
        return NORMAL_PRIORITY;

    return HIGH_PRIORITY;
}

/* Recebe um vetor de 5 caracteres
 * Atribui a sequencia à senha ou "ticket" para o paciente */
void generate_ticket(char ticket[5])
{
    int i;
    for (i = 0; i < 5; i++)
        ticket[i] = get_random(65, 90);
}
