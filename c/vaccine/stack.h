typedef struct Node
{
	char name[7];
    struct Node *next, *prev;
} Node;

typedef struct Stack
{
    Node *tail, *head;  
} Stack;

void set_name(Node *node, char name[7]);

void init_stack(Stack *stk);

int empty_stack(Stack stk);

void print_stack(Stack stk);

void deallocate_stack(Stack *stk);

int push(Stack *stk, char name[7]);

int pop(Stack *stk);