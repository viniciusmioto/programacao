typedef struct Patient
{
    int priority;
    char ticket[5];
    struct Patient *next, *prev;
} Patient;

typedef struct Queue
{
    Patient *tail, *head;  
} Queue;

void init_queue(Queue *queue);

void set_ticket(Patient *node, char ticket[5]);

void print_queue(Queue queue);

void queue_up(Queue *queue);

void deallocate_queue(Queue *queue);

int insert_beggining(Queue *queue, int p, char costumer[5]);

int insert_end(Queue *queue, int p, char costumer[5]);

int enqueue(Queue *queue, int p, char costumer[5]);

int remove_first(Queue *queue);

int empty_queue(Queue queue);

void remove_last_patient(Queue *queue, Patient *patient);