#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include "professional.h"

/* Variáveis globais para contagem da vacinação, 
 * facilitando a passagem de parâmetros para as threads */
int first_dose_done[2] = {0, 0};
int second_dose_done[2] = {0, 0};
pthread_mutex_t mutex;
Queue first_dose_queue, second_dose_queue;

/* Rotina para as Threads, recebe como argumento um profissional, verifica e aciona os procedimento de vacinação e intervalo */
void* routine(void *prof)
{  
    int last_priority, starvation;
    /* Verifica se esse profissional terminou o expediente ou se a última fila está vazia */
    while ((*(Professional*) prof).working_time < (*(Professional*) prof).employment * 60 && !empty_queue(second_dose_queue))
    {
        if ((*(Professional*) prof).working_time >= ((*(Professional*) prof).employment)*60 / 2 && (((Professional*) prof)->had_break == 0))
        {
            printf("§§ Profissional %s está em INTERVALO §§ \n\n", (*(Professional*) prof).name);
            ((Professional*) prof)->had_break = 1;
            sleep(15);
            printf("§§ %s voltou do INTERVALO §§ \n\n", (*(Professional*) prof).name);
        } 
        else
        {
            pthread_mutex_lock(&mutex);
            /* Se a fila da primeira dose ainda tiver gente */
            if (!empty_queue(first_dose_queue))
            {
                /* Mecanismo de evitar Starvation: a cada iteração existe a probabilidade de 2/10 de chamar um paciente de prioridade #10  */
                starvation = get_random(1, 10);
                if (starvation <= 2 && first_dose_queue.head->priority == HIGH_PRIORITY)
                {
                    last_priority = (first_dose_queue.tail->priority);
                    call_last((Professional*) prof, &first_dose_queue, first_dose_done, 1);
                }
                else
                {
                    last_priority = (first_dose_queue.head->priority);
                    call_next((Professional*) prof, &first_dose_queue, first_dose_done, 1);
                }
            }
            else
            {
                /* Se a fila da segunda dose ainda tiver gente */
                if (!empty_queue(second_dose_queue))
                {
                    /* Mecanismo de evitar Starvation: a cada iteração existe a probabilidade de 2/10 de chamar um paciente de prioridade #10  */
                    starvation = get_random(1, 10);
                    if (starvation <= 2 && second_dose_queue.head->priority == HIGH_PRIORITY)
                    {
                        last_priority = (second_dose_queue.tail->priority);
                        call_last((Professional*) prof, &second_dose_queue, second_dose_done, 2);
                    }
                    else
                    {
                        last_priority = (second_dose_queue.head->priority);
                        call_next((Professional*) prof, &second_dose_queue, second_dose_done, 2);
                    }
                } 
                else
                {
                    return 0;
                }
            }
            pthread_mutex_unlock(&mutex);

            sleep(last_priority/10);
        }
    }
    
    return 0;
}

int main()
{
    srand(time(NULL));
    
    Stack names_stack;
    int quantity_professionals, i;
    quantity_professionals = get_random(1, 5);
    pthread_t th[quantity_professionals];
    
    /* Inicializa os profissionais e exibe as informações destes */
    Professional professionals[quantity_professionals];
    init_professionals(professionals, &names_stack, quantity_professionals);
    show_professionals(professionals, quantity_professionals);
 
    /* Inicializa as duas filas de prioridade */
    init_queue(&first_dose_queue);
    queue_up(&first_dose_queue);
    init_queue(&second_dose_queue);
    queue_up(&second_dose_queue);
    
    /* Inicializa o mutex, o qual é utilizado para fechar a thread ao chamar um paciente
     * Evita que o mesmo paciente seja chamado por dois ou mais atendentes ao mesmo tempo */
    pthread_mutex_init(&mutex, NULL);

    /* Criação das threads na quantidade de profissionais disponíveis */
    for (i = 0; i < quantity_professionals; i++)
    {
        if (pthread_create(th + i, NULL, &routine, professionals+i) != 0)
            perror("Failed to create thread");

        /* printf("Thread %d has started\n", i); */
    }
    
    for (i = 0; i < quantity_professionals; i++)
    {
        if (pthread_join(th[i], NULL) != 0)
            perror("Failed to join thread");
        
        printf("--- %s finalizou o expediente --- \n", professionals[i].name);
    }

    pthread_mutex_destroy(&mutex);
    close_place(first_dose_done, second_dose_done, first_dose_queue, second_dose_queue, professionals, quantity_professionals);
    deallocate_queue(&first_dose_queue);
    deallocate_queue(&second_dose_queue);
    deallocate_stack(&names_stack);
    return 0;
}