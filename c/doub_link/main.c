#include <stdio.h>
#include <stdlib.h>

typedef struct Node
{
    int value;
    struct Node *next, *prev;
} Node;

typedef struct List
{
    Node *tail, *head;  
} List;

void deallocate(Node **head, Node **tail)
{
	Node *curr = *head;

	if (head == NULL)
		return;

	while (curr->next != NULL)	
	{
		curr = curr->next;
		free(curr->prev);
	}

	free(curr);
	*tail = NULL;
	*head = NULL;
}

void insert_beggining(Node **head, int x)
{
	Node *new_node = malloc(sizeof(Node));

	if (new_node == NULL)
		return;

	new_node->value = x;
	new_node->prev = NULL;
	new_node->next = *head;

	if (*head != NULL)
		(*head)->prev = new_node;

	*head = new_node;
}

void insert_end(Node **tail, int x)
{
	Node *new_node = malloc(sizeof(Node));

	if (new_node == NULL)
		return;

	new_node->value = x;
	new_node->next = NULL;
	new_node->prev = *tail;
	(*tail)->next = new_node;
	*tail = new_node;
}

void insert_after(Node* node, int x)
{
	Node *new_node = malloc(sizeof(Node));

	if (new_node == NULL)
		return;

	new_node->value = x;	
	new_node->prev = node;
	new_node->next = node->next;

	if (node->next != NULL)
		node->next->prev = new_node;

	node->next = new_node;
}

void remove_element(Node *node)
{
	if (node->prev != NULL)
		node->prev->next = node->next;
	
	if (node->next != NULL)
		node->next->prev = node->prev;
	
	node->next = NULL;
	node->prev = NULL;
	free(node);	
}

Node *find_node(Node* head, int x)
{
	Node *curr;
	
	for (curr = head; curr != NULL; curr = curr->next)
		if (curr->value == x)
			return curr;

	return NULL;
}

void print_list(Node *head)
{
	Node *curr;

	for (curr = head; curr != NULL; curr = curr->next)
		printf("%d ", curr->value);

	printf("\n");
}

void init_list(Node **head, Node **tail, int x)
{
	Node *new_node = malloc(sizeof(Node));

	if (new_node == NULL)
		return;

	new_node->value = x;
	new_node->prev = NULL;
	new_node->next = NULL;
	*head = new_node;
	*tail = new_node;

}

int main()
{
    Node *head = NULL;
	Node *tail = NULL;

	init_list(&head, &tail, 7);
	insert_beggining(&head, 3);
	insert_beggining(&head, 1); 
	insert_end(&tail, 5);
	insert_after(head->next, 4);
	remove_element(head->next);

	Node *found = find_node(head, 4);
	if (found != NULL)
		printf("value: %d, next: %p\n", found->value, found->next);
	else 
		printf("Node not found\n");

	print_list(head);
	deallocate(&head, &tail);
	printf("\n");

    return 0;
}
