#include <stdio.h>
#include <stdbool.h>

/* recebe o tamanho e um vetor e le todos os elementos desse */
void read_sequence(int length, int array[])
{
  int i;
  printf("enter the array: ");
  for(i = 0; i < length; i++) {
    scanf("%d", &array[i]);
  }
}

/* recebe duas sequências de inteiros, retorna true se forem iguais, e false caso contrário */
bool verify_sequence(int length, int array1[], int array2[])
{
  int i;
  for(i = 0; i < length; i++) {
    if (array1[i] != array2[i])
      return false;
  }
  return true;
}

int main(void)
{
  int length;
  printf("enter the length: ");
  scanf("%d", &length);
  int array1[length], array2[length];

  read_sequence(length, array1);
  read_sequence(length, array2);
  if (verify_sequence(length, array1, array2))
    printf("sim\n");
  else
    printf("nao\n");

  return 0;
}
