#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

int primes[10] = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29};

void *routine(void *arg)
{
    printf("%d ", *(int*) arg );
}

int main()
{
    pthread_t th[10];
    int i;

    for (i = 0; i < 10; i++)
    {    
        if (pthread_create(&th[i], NULL, &routine, primes+i) != 0)
            perror("Failed to create thread");
    }

    for (i = 0; i < 10; i++)
        if (pthread_join(th[i], NULL) != 0)
            perror("Failed to join thread");

    printf("\n");
    return 0;
}