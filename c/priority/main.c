#include "double_linked_list.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

/* preenche uma senha ou "ticket" para o cliente */
void set_costumer_ticket(char costumer[3])
{
    int i;
    for (i = 0; i < 3; i++)
        costumer[i] = (rand() % (26)) + 65;
}

/* preenche a fila com um núm aleatório de clientes antes do atendimento começar */
void queue_up(Queue *queue)
{
    int i, priority;
    int first_costumers = ((rand() % 10) + 2);
    char costumer[3];

    for (i = 0; i < first_costumers; i++)
    {
        set_costumer_ticket(costumer);
        priority = ((rand() % 5) + 1);
        insert_position(queue, priority, costumer);
    }
}

/* chama o próx cliente e adiciona um ao final da fila 
 * seria interessante também inserir um núm aleatório de clientes no final da fila... */
void call_next(Queue *queue, int priority_done[5], int *process_time)
{
    int priority;
    char costumer[3];

    if (empty_list(*queue) || *process_time >= 30)
        return;

    print_list(*queue);
    sleep(queue->head->priority);
    priority_done[queue->head->priority - 1]++;
    *process_time += queue->head->priority;
    remove_first(queue);
    priority = ((rand() % 5) + 1);
    set_costumer_ticket(costumer);
    insert_position(queue, priority, costumer);
    call_next(queue, priority_done, process_time);
}

/* imprime quantos clientes de cada prioridade foram atendidos */
void close_place(int priority_done[5], int process_time)
{
    printf("\n+++ FIM DO EXPEDIENTE +++\n");
    int i;
    for (i = 0; i < 5; i++)
        printf("Prioridade #%d: %d\n", i+1, priority_done[i]);

    printf("Total de expediente: %d segundos\n", process_time);

}

int main()
{
    srand(time(NULL));
    Queue queue;
    int priority_done[5] = {0, 0, 0, 0, 0};
    int process_time = 0;
    
    init_list(&queue);
    queue_up(&queue);
    call_next(&queue, priority_done, &process_time);
    deallocate(&queue);
    close_place(priority_done, process_time);

    return 0;
}
