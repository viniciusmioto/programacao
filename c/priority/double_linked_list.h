typedef struct Node
{
    int priority;
    char costumer[3];
    struct Node *next, *prev;
} Node;

typedef struct Queue
{
    Node *tail, *head;  
} Queue;

void init_list(Queue *queue);

void print_list(Queue queue);

void deallocate(Queue *queue);

int insert_beggining(Queue *queue, int p, char costumer[3]);

int insert_end(Queue *queue, int p, char costumer[3]);

int insert_position(Queue *queue, int p, char costumer[3]);

int remove_first(Queue *queue);

int empty_list(Queue queue);