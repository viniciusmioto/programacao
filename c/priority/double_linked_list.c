#include <stdio.h>
#include <stdlib.h>

typedef struct Node
{
    int priority;
	char costumer[3];
    struct Node *next, *prev;
} Node;

typedef struct Queue
{
    Node *tail, *head;  
} Queue;

void costumer_ticket(Node *node, char costumer[3])
{
	int i;
	for (i = 0; i < 3; i++)
		node->costumer[i] = costumer[i];
}

/* Cria uma fila vazia, isto é, head e tail apontam para NULL; */
void init_list(Queue *queue)
{
    queue->head = NULL;
    queue->tail = NULL;
}

/* Retorna 1 se a fila está vazia e zero caso contrário; */
int empty_list(Queue queue)
{
    return queue.head == NULL;
}

/* Imprime os elementos da fila, do início ao final. Se ela for vazia não imprime nada */
void print_list(Queue queue)
{
	Node *curr;

	for (curr = queue.head; curr != NULL; curr = curr->next)
		printf("#%d |%s| ", curr->priority, curr->costumer);

	printf("\n");
}

/* Remove todos os elementos da fila e faz com que head e tail apontem para NULL; */
void deallocate(Queue *queue)
{
	Node *curr = queue->head;

	if (queue->head == NULL)
		return;

	while (curr->next != NULL)	
	{
		curr = curr->next;
		free(curr->prev);
	}

	free(curr);
	queue->tail = NULL;
	queue->head = NULL;
}

/* Insere o elemento p no início da fila; Retorna 1 se a operação foi bem sucedida e zero caso contrário; */
int insert_beggining(Queue *queue, int p, char costumer[3])
{
	Node *new_node = malloc(sizeof(Node));

	if (new_node == NULL)
		return 0;

	new_node->priority = p;
	new_node->prev = NULL;
	costumer_ticket(new_node, costumer);

	if (!empty_list(*queue)) 
    {
        new_node->next = queue->head;
        (queue->head)->prev = new_node;
        queue->head = new_node;
    }
    else
    {
	    new_node->next = NULL;
	    queue->head = new_node;
	    queue->tail = new_node;
    }
  
    return 1;
}

/* Insere o elemento p no final da fila; Retorna 1 se a operação foi bem sucedida e zero caso contrário; */
int insert_end(Queue *queue, int p, char costumer[3])
{
	Node *new_node = malloc(sizeof(Node));

	if (new_node == NULL)
		return 0;

	if (empty_list(*queue))
	{
		free(new_node);
		return insert_beggining(queue, p, costumer);
	}

	new_node->priority = p;
	costumer_ticket(new_node, costumer);
	new_node->next = NULL;
	new_node->prev = queue->tail;
	(queue->tail)->next = new_node;
	queue->tail = new_node;
    return 1;
}

/* Função auxiliar, insere o elemento p após um nodo especificado. Retorna 1 se a operação foi bem sucedida e zero caso contrário; */
int insert_position(Queue *queue, int p, char costumer[3])
{
	Node *curr;
	Node *new_node = malloc(sizeof(Node));

	/* caso esse seja o primeiro elemento da fila */
	if (empty_list(*queue) || p > queue->head->priority)
	{
		free(new_node);
		return insert_beggining(queue, p, costumer);
	}

	/* caso seja o último da fila  */
	if (p <= queue->tail->priority) 
	{
		free(new_node);
		return insert_end(queue, p, costumer);
	}
	
	if (new_node == NULL)
		return 0;	

	for (curr = queue->head; curr->priority >= p; curr = curr->next);

	new_node->priority = p;
	costumer_ticket(new_node, costumer);
	new_node->prev = curr->prev;
	new_node->next = curr;
	curr->prev = new_node;
	(new_node->prev)->next = new_node;

    return 1;
}

/* Remove o primeiro elemento da fila. Retorna 1 se a operação foi bem sucedida e zero caso contrário; */
int remove_first(Queue *queue)
{
	Node *curr = queue->head;

	if (empty_list(*queue))
		return 0;

	if (queue->head->next == NULL)
	{
		deallocate(queue);
		return 1;
	}
	
	queue->head = queue->head->next;
	queue->head->prev = NULL;
	curr->prev = NULL;
	curr->next = NULL;	
	free(curr);	
	return 1;
}