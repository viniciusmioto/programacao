/* Vinícius Mioto - BCC - GRR20203931 */
#include <stdio.h>
#include <stdlib.h>

typedef struct  t_nodo
{
    int valor;
    struct t_nodo *prox;
} t_nodo;

typedef struct t_lista
{
    t_nodo *inicio;
    int tamanho;
} t_lista;

/* Cria uma lista vazia, isto é, aponta para NULL e contém tamanho zero; Retorna 1 se a operação foi bem sucedida e zero caso contrário; */
int cria_lista(t_lista *l)
{   
    l->inicio = NULL;
    l->tamanho = 0;
    return 1;
}

/* Retorna 1 se a lista está vazia e zero caso contrário; */
int lista_vazia(t_lista *l)
{
    return l->inicio == NULL;
}

/* Remove todos os elementos da lista e faz com que ela aponte para NULL; */
void destroi_lista(t_lista *l)
{
    t_nodo *atual;

    if (lista_vazia(l))
        return;

    /* O último nodo deve apontar para NULL */
    while (l->inicio->prox != NULL)
    {
        atual = l->inicio;
        l->inicio = l->inicio->prox;
        free(atual);
    }

    /* elimina o nodo remanescente */
    free(l->inicio);
    l->tamanho = 0;
    l->inicio = NULL;
}

/* Insere o elemento x no início da lista; Retorna 1 se a operação foi bem sucedida e zero caso contrário; */
int insere_inicio_lista(int x, t_lista *l)
{
    t_nodo *novo_nodo = malloc(sizeof(t_nodo));

    /* se não for possível fazer o malloc */
    if (novo_nodo == NULL)
        return 0;

    novo_nodo->valor = x;
    novo_nodo->prox = l->inicio;

    l->tamanho++;
    l->inicio = novo_nodo;
    return 1;
}

/* Insere o elemento x no final da lista; Retorna 1 se a operação foi bem sucedida e zero caso contrário; */
int insere_fim_lista(int x, t_lista *l)
{
    t_nodo *atual = l->inicio;
    t_nodo *novo_nodo = malloc(sizeof(t_nodo));

    if (lista_vazia(l))
    {
        free(novo_nodo);
        return insere_inicio_lista(x, l);
    }
    
    /* novo_nodo será inserido no fim */
    novo_nodo->prox = NULL;
    novo_nodo->valor = x;

    while (atual->prox != NULL)
    {
        atual = atual->prox;
    }

    atual->prox = novo_nodo;
    
    l->tamanho++;
    return 1;
}

/* Função auxiliar, insere o elemento x após um nodo especificado. Retorna 1 se a operação foi bem sucedida e zero caso contrário; */
int insere_apos(int x, t_nodo *nodo, t_lista *l)
{
    t_nodo *novo_nodo= malloc(sizeof(t_nodo));
    
    if (novo_nodo == NULL)
        return 0;

    /* insere apos o nodo especificado, aleterando os ponteiros */
    novo_nodo->valor = x;
    novo_nodo->prox = nodo->prox;
    nodo->prox = novo_nodo;
    l->tamanho++;
    return 1;
}

/* Insere o elemento x na lista de maneira que ela fique em ordem crescente, do início para o final dela; Retorna 1 se a operação foi bem sucedida e zero caso contrário; */
int insere_ordenado_lista(int x, t_lista *l)
{
    t_nodo *atual = l->inicio;
   
    /* caso esse seja o primeiro elemento da lista */
    if (lista_vazia(l) || (l->inicio)->valor >= x)
    {
        insere_inicio_lista(x, l);
        return 1;
    }

    while (atual->prox != NULL)
    {
        if (atual->prox->valor >= x)
            break;

        atual = atual->prox;
    }
    insere_apos(x, atual, l);

    return 1;
}

/* Imprime os elementos da lista, do início ao final. Se ela for vazia não imprime nada, nem um \n; */
void imprime_lista(t_lista *l)
{
    t_nodo *atual;
    
    if (lista_vazia(l))
        return;

    for (atual = l->inicio; atual != NULL; atual = atual->prox)
        printf("%d ", atual->valor);
    printf("\n");
}

/* Retorna 1 se o elemento contendo a chave chave existe na lista, caso contrário retorna zero; */
int pertence_lista(int chave, t_lista *l)
{
    t_nodo *nodo;
    int i = 1;
    
    nodo = l->inicio;
    while(nodo != NULL && nodo->valor != chave)
    {
        nodo = nodo->prox;
        i++;
    }

    /* encontrou o elemento no nodo */
    if (nodo != NULL)
        return i;
    
    return 0;
}

/* Remove o primeiro elemento da lista e o retorna em *item; Retorna 1 se a operação foi bem sucedida e zero caso contrário; */
int remove_primeiro_lista(int *item, t_lista *l)
{
    t_nodo *para_remover;

    if (lista_vazia(l))
        return 0;

    para_remover = l->inicio;

    /* o inicio de l passa a apontar para  o que era o 2º elemento */
    l->inicio = (l->inicio)->prox;
    *item = para_remover->valor;
    free(para_remover);
    l->tamanho--;
    return 1;
}

/* Remove o último elemento da lista e o retorna em *item; Retorna 1 se a operação foi bem sucedida e zero caso contrário; */
int remove_ultimo_lista(int *item, t_lista *l)
{
    t_nodo *para_remover;
    t_nodo *atual;

    if (lista_vazia(l))
        return 0;

    /* se a lista só tiver um elemento, remove o primeiro */
    if (l->tamanho == 1) 
        return remove_primeiro_lista(item, l);

    atual = l->inicio;
    while (atual->prox->prox != NULL)
    {
        atual = atual->prox;
    }

    /* nodo para_remover será liberado, e o novo último apontará para NULL */
    para_remover = atual->prox;
    atual->prox = atual->prox->prox;
    *item = para_remover->valor;
    free(para_remover);
    l->tamanho--;
    return 1;

}

/* Se o elemento chave existir na lista, o retorna em *item; Retorna 1 se a operação foi bem sucedida e zero caso contrário (elemento não encontrado também retorna zero); */
int remove_item_lista(int chave, int *item, t_lista *l)
{
    t_nodo *para_remover;
    t_nodo *atual;

    /* se a lista estiver vazia ou o elemento não pertence a lista */
    if (lista_vazia(l) || pertence_lista(chave, l) == 0)
        return 0;

    /* se o  elemento procurado for o primeiro */
    if ((l->inicio)->valor == chave)
    {
        remove_primeiro_lista(item, l);
        return 1;
    }

    /* percorre a lista até encontrar o elemento */
    for (atual = l->inicio; atual->prox != NULL; atual = atual->prox)
    {
        /* elemento encontrado no prox nodo */
        if (atual->prox->valor == chave)
        {
            para_remover = atual->prox;
            
            atual->prox = atual->prox->prox;
            *item = para_remover->valor;
            free(para_remover);
            l->tamanho--;
            return 1;
        }
    }
    return 0;
}

/* Concatena os elementos da lista m (na mesma ordem) ao final da lista l. Faz a lista m conter o valor NULL; */
int concatena_listas(t_lista *l, t_lista *m)
{
    t_nodo *atual;
    
    /* insere os elemento de m no final de l */
    for (atual = m->inicio; atual != NULL; atual = atual->prox)
        insere_fim_lista(atual->valor, l);

    destroi_lista(m);
    return 1;
}

/* Cria uma nova lista m contendo uma cópia exata da lista l. Retorna 1 se a operação foi bem sucedida e zero caso contrário. */	
int copia_lista(t_lista *l, t_lista *m)
{
    t_nodo *atual;

    /* insere os elemento de l em m */
    for (atual = l->inicio; atual != NULL; atual = atual->prox)
    {
        insere_fim_lista(atual->valor, m);
    }

    return 1;
}

