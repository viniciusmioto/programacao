#include <stdio.h>

void swap(int *a, int *b)
{
    int aux = *a;
    *a = *b;
    *b = aux;
}

int main()
{
    int a, b;

    a = 2;
    b = 8;

    printf("a = %d \nb = %d \n", a, b);
    swap(&a, &b);
    printf("a = %d \nb = %d \n", a, b);

    return 1;
}


