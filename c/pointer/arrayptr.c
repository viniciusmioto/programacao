#include <stdio.h>

int main()
{
    int numbers[5];
    int *ptr = numbers; // &numbers[0]
    int v = 2;

    for(;ptr < &numbers[5]; ptr++)
    {
        *ptr = v;
        v = v * 2;
    }

    for(int i = 0; i < 5; i++)
    {
        printf("%d ", numbers[i]);
    }

    printf("\n");
}
