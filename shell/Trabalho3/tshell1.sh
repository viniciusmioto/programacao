#!/bin/bash

# verifica no local $1 se $2 é um diretório, se é do tipo oculto ou normal e então faz pushd para o dir
verify_dir() 
{
        if [ -d "$1/$2" ]
        then
        #       echo "normal directory"
		pushd "$LOCATION/$2"
		return 0
        elif [ -d "$1/.$2" ]
        then
        #       echo "hidden directory"
		pushd "$LOCATION/.$2"
		return 0
        else
        #	echo "not a directory"
		return 1
fi
}

find_line()
{
	FILE_SIZE=$(du -b $1 | awk {'print $1'})
	LINE=$((${FILE_SIZE}/2000000 - 43))
}

LOCATION=/home/html/inf/cursos/ci1001/tshell1 	
pushd $LOCATION 	
var1=$(cat README)				# 2
var2=$(cut -d';' -f3 "./$var1" | head -n 1)	# 3
var3=$(du -b * | grep $var2 | cut -f2)		# 4 erro grep $var2
var4=$(cat "./$var3")				# 5
verify_dir "$LOCATION" "$var4"			# 6
var5=$(cat $(file * | grep "ASCII" | grep -v "executable" | grep -v "script" | cut -d":" -f1))			# 7 debian
popd						# volta do back_velho
find_line "senha.gpg"				# 8.1
PASSWORD=$(nice -13 gpg --batch --yes --passphrase $var5 -d senha.gpg | sed ${LINE}'!d' | grep -o "[AEIOU]" | tr -d '\n' | tr 'AEIOU' '12345')	# talvez renice?		# 8.2
echo "A senha da casa eh: $PASSWORD" 2>&1 | tee ~/tmp/resultado.txt

