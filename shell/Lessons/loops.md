# For

## First type

```bash
for name [in list]
do 
    list
done
```

```bash
for name [in list] ; do list ;  done
```

## Second type

```bash
for (( expr1; expr2; expr3 ))
do
    list
done
```

```bash
for (( expr1; expr2; expr3 )) ; do list ;  done
```

## Example

```bash
IFS=:

for dir in $PATH
do
    ls -ld $dir
done
```

`IFS=:` changes the separator from / to :

# While

```bash
while condition
do 
    sentences
done
```