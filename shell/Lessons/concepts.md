# Other Concepts

## Command Substitution

We can assign a command output to a variable `VARIABLE=$command`

```bash
students=$(ls /home/bcc)
```

## "Boolean"

There is a command that return 0 (true) or 1 (false), which is `[ expr ]`

* `[ -a file ]` tests if the *file* exists
* `[ -d file ]` tests if the *file* is a directory
* `[ -f file ]` tests if the *file* exists and is a regular file
* `[ -h file ]` tests if the *file* exists and is a symbolic link

## Lists

A lists is a sequence of commands with one or more pipelines:
* `;`, `&`, `&&`, `||`

And it could end with:
* `;`, `&`, `<newline>`

```bash
mkdir temp ; cd temp
```

creates a directory named 'temp' and goes into it

```bash
firefox & vi test.pas
```

opens Firefox in background and open test.pas in VI in terminal

```bash
ls && echo ok
```

The Shell will execute 'echo ok' only if the 'ls' return 0 (when everything goes ok)

```bash
ls /root && echo ok
```

In this case we will not get 'echo ok'

```bash
ls /root || echo ok
```

It will give us 'ok' because the 'ls' went wrong.

```bash
ls || echo ok
```

It will list the files in the directory, but it won't show 'ok', because `||` is an exclusive or

## Using special commands

```bash
user@linux:~/Documents$ mkdir temp
user@linux:~/Documents$ [ -d temp ]
user@linux:~/Documents$ echo $?
0
```

```bash 
user@linux:~/Documents$ [ ! -d new_dir ] && echo ok
ok
```

### "if then else" command

```bash
user@linux:~/Documents$ [ -d my_dir ] && echo 'ok' || echo 'not ok'
not ok
```

## Overview

* `&&` is an AND
    * command1 && command2
    * command2 will be executed if command1 returned 0 (true)

* `||` is an OR
    * command1 || command2
    * command2 will be executed if command1 return 1 (false)