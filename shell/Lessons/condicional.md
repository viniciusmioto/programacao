# Conditions

## if/else
```bash
if condition
then
    command
elif condition
then
    command
else
    command
fi
```

The condition will return either 0 (true) or a values in [1, 255] (false)

```bash
#!/bin/bash

FILENAME=$1
WORD1=$2
WORD2=$2

if grep $WORD1 $FILENAME || grep $WORD2 $FILENAME
then
    echo "$WORD1 or $WORD2 is in $FILENAME"
fi
```

## case

```bash
case expr in
    pattern1 )
        sentences ;;
    pattern2 )
        sentences ;;
esac
```
