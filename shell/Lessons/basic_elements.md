# Basic Elements of Bash Shell

## commands arguments

* the first word is the command that will be executed
* all the rest are the arguments i.e. how and where the command will operate

### Examples

* mail user@ufpr.br   
    
    * this command will send an e-mail to user@ufpr.br

* lpr -P[office]() file.pdf

    * the lpr will print the file.pdf in the print named "office"

## Arguments

Arguments can be files or options for the command. They chance the behavior of the command.

### ls -a: list everything, including the hidden files
```bash
user@ufpr:~/prog$ ls -a
.   ci1001.pdf            .git             README.md
..  elementos_basicos.md  livro_prog1.pdf  shell
```

### ls -l: list and show the files information
```bash
user@ufpr:~/prog$ ls -l
total 1568
-rw-rw-r-- 1 user user     864 may  9 10:12 basic_elements.md
-rw-rw-r-- 1 user user   76704 apr 26 21:22 ci1001.pdf
-rw-rw-r-- 1 user user 1513448 apr 27 10:03 livro_prog1.pdf
-rw-rw-r-- 1 user user      25 apr 23 15:52 README.md
drwxrwxr-x 2 user user    4096 may  9 09:25 shell
```

## ls -ltr: it does the same as the previous, but in reverse order
```bash
user@ufpr:~/prog$ ls -ltr
total 1568
-rw-rw-r-- 1 user user      25 apr 23 15:52 README.md
-rw-rw-r-- 1 user user   76704 apr 26 21:22 ci1001.pdf
-rw-rw-r-- 1 user user 1513448 apr 27 10:03 livro_prog1.pdf
drwxrwxr-x 2 user user    4096 may  9 09:25 shell
-rw-rw-r-- 1 user user    1115 may  9 10:14 basic_elements.md
```

## Files
* Regulars: ASCII readable (human and computer)
    * ASCII, PDF, MP3...
* Executable: readable in ASCII or not (binaries)
    * bash, lpr, mail, libreoffice, firefox
    * shell scripts, python scripts, etc
* Directories: files that have other files inside
    * your HOME (~), ~/Documents, ~/Downloads
    * /bin, /usr/bin,
    * /var, /var/log
    * ., ..: special directories

## Directories Hierarchy in UNIX

* `/` root
* `/etc` configuration files
* `/usr` distribution files
* `/usr/bin` distribution binaries
* `/lib` fundamental system libraries
* `/dev` system devices (disk, bashs, etc)

### Navigation commands
* `cd ~` changes directory to /home/, you can use just `cd`
* `pwd` shows the current directory
* `cd ..` changes current directory to one level up
* `cd -` changes current directory to the directory that you were last in

```bash
user@ufpr:~/prog$ cd ~
user@ufpr:~$: cd /Documents/Study/
user@ufpr:~$/Documents/Study: cd ..
user@ufpr:~$/Documents: pwd
/home/user/Documents
user@ufpr:~$/Documents: cd ~ /Downloads/
user@ufpr:~$/Downloads: cd -
/home/user/Documents
user@ufpr:~$/Documents:
```
### Other navigation commands

* `pushd` echos your destination directory and your point of origin. This is your directory stack
* `popd` will first **cd** you into the directory record which is on top of the stack then and remove the documentation (remove it from the dir stack)

```bash
user@ufpr:~$ pushd Documents/GitLab/
~/Documents/GitLab ~
user@ufpr:~/Documents/GitLab$ pushd ~/Downloads/
~/Downloads ~/Documents/GitLab ~
user@ufpr:~/Downloads$ pushd /tmp/
/tmp ~/Downloads ~/Documents/GitLab ~
user@ufpr:/tmp$ popd
~/Downloads ~/Documents/GitLab ~
user@ufpr:~/Downloads$ popd
~/Documents/GitLab ~
user@ufpr:~/Documents/GitLab$ popd
~
user@ufpr:~$ popd
bash: popd: directory stack empty
user@ufpr:~$ 
```