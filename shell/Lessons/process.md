# Process

Basic commands

* `who` shows all users logged in the system
* `ps` shows the  process you are  running in your Shell
* `ps aux` shows all the process running in the system
* `ps aux | grep user` shows the process of an specific user
* `pstree` show a tree of all process
 
## Foreground

* `jobs` shows the process running in your Shell

When we run a command like `firefox` in the terminal, the Shell will open the Browser for us, but we can add any more commands until we close Firefox process

`fg 1` starts the first paused process in foreground

## Background

When we run `firefox &` in the terminal, the Shell will open the Browser and we can still use the terminal 

`bg 1` starts the first  paused process in foreground

## Input and Output in Background

* We shouldn't use I/O in background jobs
* If there is an input, the job will wait for it
* If there is an output, your Shell will be a mess with the messages
* That's why we should use I/O redirecting

`command < input > output`

## Control Commands

* `CTRL C` finish the current command
* `CTRL D` finish input
* `CTRL \` stop the current command
* `CTRL S` stop the screen output
* `CTRL Q` restart the screen output
* `DEL` or `CTRL ?` delete character
* `CTRL U` delete all command line
* `Ctrl Z` pauses the process



