# Wildcards and Expansion

## Wildcards

We can access files by using wildcards, when we know that there are files in the directory, but we don't know how many or which files are there.

* `*` any string of character
* `?` any single character

### Examples
* `ls *.c` expands all files that end with ".c"
* `ls m*s` all files that start with "m" and end with "s"
* `ls m?s` all files that has 3 characters in the name, start with "m" and end with "s"
* `ls *.*` all files with a dot in the name
* `echo *` all files not hidden (like an **ls**)

```bash
user@ufpr:~/tests$ ls
file.tex  i1.jpg  i2.jpg  i3.jpg  i4.jpg  i5.jpg  prog.c  script.py  src
user@ufpr:~/tests$ ls *
file.tex  i1.jpg  i2.jpg  i3.jpg  i4.jpg  i5.jpg  prog.c  script.py

src:
app.js  style.css
user@ufpr:~/tests$ ls *s*
script.py

src:
app.js  style.css
```

## Expansion with Square Brackets []
* `[set]` expands for all symbols in *set*
* `[!set]` expands for all that are not in *set*

### Examples
* `ls [xyz]` x, y or z
* `ls [xyz]*` any file that starts with x, y or z
* `ls [a-z]` any lower-case 

```bash
user@ufpr:~/tests$ ls i[1-6]*
i1.jpg  i2.jpg  i3.jpg  i4.jpg  i5.jpg
user@ufpr:~/tests$ ls i[1-3]*
i1.jpg  i2.jpg  i3.jpg
user@ufpr:~/tests$ ls *[xy]
file.tex  script.py
user@ufpr:~/tests$ ls *[!xy]
i1.jpg  i2.jpg  i3.jpg  i4.jpg  i5.jpg  prog.c

src:
app.js  style.css
```
## Expansion with Curly Brackets {}

* `{set}` expands for a specified *set*

### Examples
* `echo mari{a,o}` will show "maria" and "mario"
* `touch file{1,2,3}test` will create the files "file1test", "file2test", "file3test" 
* `echo a{r{O,U},va}s` will show "arOs arUs avas"

```bash
user@ufpr:~/tests$ ls
file.tex  i1.jpg  i2.jpg  i3.jpg  i4.jpg  i5.jpg  prog.c  script.py  src
user@ufpr:~/tests$ mv i1.jpg new_name.jpg
user@ufpr:~/tests$ ls
file.tex  i2.jpg  i3.jpg  i4.jpg  i5.jpg  new_name.jpg  prog.c  script.py  src
```