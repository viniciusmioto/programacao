# Permissions

The UNIX system gives permissions for all files and directories, with different modes for the root, group and every other user.

The command `chmod` changes the permissions of a file, on the other hand the command `umask` defines the mask of a standard permission for a user. To see the permissions we use `ls -l`

```bash
-rw-rw-r-- 1 user user      25 abr 23 15:52 README.md
drwxrwxr-x 2 user user    4096 mai 14 15:29 shell
```

The first character can be a "d" for directories or a "-" for normal files. The next 3 characters show the permissions for the root, then there are 3 characters for the group, and finally the permission for all other users in the last 3.

* **r**: read. In directories we can see the files inside with `ls`. In files we can read the content;
* **w**: write. In directories we can create and delete files. In files we can update the content;
* **x**: execute. In directories we can run Shell commands like `cd`. In files we can run a script, for example.
* X, s, t: other permissions

## chmod

* `chmod g-rwx file` gives "read, write and execute" permissions for the **group**
* `chmod 700 file` give "read, write and execute" for the **root** and nothing to the group others 