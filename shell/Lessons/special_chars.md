# Special Characters

The Shell has many special characters, if we want to use some of those we need to quoting it by using ', or " or \ . For example if we use `echo *` the command will show all the files in the current directory, but if we want to show '*', we need to use `echo \*`

If we want to show the string "2 * 3 > 5" and use `echo 2 * 3 > 5` it will not work, actually the Shell will write "2", then it will show all the files in the directory *, and will change the output to a file named as "5". To avoid this behavior, we can use `echo "2 * 3 < 5"`

* `'` strong quote
* `"` weak quote

```bash
user@ufpr:~/Documents/Prog$ echo '$PATH'
$PATH
user@ufpr:~/Documents/Prog$ echo "$PATH"
/home/user/.node_modules_global/bin:/home/user/anaconda3/bin:/home/user/anaconda3/condabin:/home/user/ [...]
```     

If we want to use a *csv* file in Shell we need to use quotes, because the columns of these files normally are divide by ';', as the command `cut -d';' -f2 data.csv`