# Variables

### Declaration: NAME=value
* USER=someone
* USERNAME="Someone Else"

### To access the value of a variable
We need to use `$` before the variable name
* echo $USER
* echo ${USERNAME}
* echo The name of the ${USER} is ${USERNAME}

### To delete a variable
We need to unse `unset` VARIABLE
Any variable that doesn't exist in Shell is an empty string

### Quoting
```bash
tux@ufpr:~$ echo $USER
tux
tux@ufpr:~$ echo "$USER"
tux
tux@ufpr:~$ echo '$USER'
$USER
```

When we use a weak quoting with ", the Shell will print the value of $USER, in this case is "tux", but if we use ' it will show exactly what is quoted, in this case $USER.

```bash
tux@ufpr:~$ STRING="string       with    spaces"
tux@ufpr:~$ echo $STRING
string with spaces
tux@ufpr:~$ echo "$STRING"
string       with    spaces
```

Shell will ignore the spaces in a string if we don't use quotes

## Variable types
* **Local Variables** − A local variable is a variable that is present within the current instance of the shell. It is not available to programs that are started by the shell. They are set at the command prompt.

* **Environment Variables** − An environment variable is available to any child process of the shell. Some programs need environment variables in order to function correctly. Usually, a shell script defines only those environment variables that are needed by the programs that it runs. To see all environment variables we use `env`

* **Shell Variables** − A shell variable is a special variable that is set by the shell and is required by the shell in order to function correctly. Some of these variables are environment variables whereas others are local variables.

## Subprocess

To use a local variable that we defined in a subshell, we need to use `export`, so the subprocess will know which variable we are trying to use