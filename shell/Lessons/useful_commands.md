# Useful Commands

* `man` shows a manual of a Shell command
* `bc` language that supports a calculator
* `cat` concatenates files and print in the standard output
* `cp` copy files and directories
* `date` prints or define the data/hour of the system
* `diff` compares files line by line
* `file` shows the file extension (type)
* `find` searches for a file in the directory hierarchy
* `finger` shows information of a login name
* `grep` prints lines that macth with a parameter
* `head` shows the first part of a file
* `tail` shows the last part of a file
* `less` shows the file with a pause
* `mv` renames a file
* `sort` sorts a text file
* `touch` creates a file
* `wc` shows the number of lines, columns and characters
