# Scripts and Functions

## Script

We can use Shell "scripts" in the command line as we saw before

```bash
tux@ufpr:~$ mkdir temp
tux@ufpr:~$ chmod 700 temp
tux@ufpr:~$ ls -ld temp
drwx------ 2 tux bcc 4096 mai 25 10:34 temp
```

Usually, we define a script as a ASCII file with Sheel commands that can be executed, and this will run all the commands inside. Basically, it is a program in Shell

We can create a file named "my_script.sh" with the commands we used before

my_script.sh
```bash
mkdir temp
chmod 700 temp
ls -ld temp
```

To run the script we `source` because the script is not executable yet
```bash
tux@ufpr:~$ source my_script.sh 
drwx------ 2 tux bcc 4096 mai 25 10:38 temp
```

my_script has created a directory named "temp" and changed the permissions to 700, finally it list the directory

To make my_script.sh executable we user `chmod`

```bash
tux@ufpr:~$ rmdir temp
tux@ufpr:~$ chmod u+x my_script.sh 
tux@ufpr:~$ ./my_script.sh 
drwx------ 2 tux bcc 4096 mai 25 10:43 temp
```

We executed the script with `./` because we didn't need to use `source` anymore

### Hashbang

We use hashbang to make sure the script will execute the language we choose, in this case ins Bash

my_script.sh
```bash
#!/bin/bash
mkdir temp
chmod 700 temp
ls -ld temp
```

We can use it for Python, Perl, ZShell, etc.

## Functions

A function is a subprogram that is stored in the memory and it can be used later by a Shell or for another function

There are two ways to declare a function. The first one is using the word `function`

```bash
function create_dir_temp
{
    mkdir temp
    chmod 700 temp
}
```

The other way is use the name followed by `()`

```bash
create_dir_temp()
{
    mkdir temp
    chmod 700 temp
}
```

To call a function in our script we need to use `funct_name()`
```bash
mensagem() 
{
    echo fim
}

# comment
mkdir temp
cd temp
mensagem()
```

To remove a function we use `unset -f function_name`