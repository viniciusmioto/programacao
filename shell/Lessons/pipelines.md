# Inputs and Outputs

The standard input in UNIX is the keyboard, the standard output is the screen

The `cat` command reads the input lines and print in the standard output

```bash
user@ufpr:~/tests$ cat
hello
hello
bye
bye
^D
```

We can print whatever is in a file

```bash
user@ufpr:~/tests$ cat script.py 
print('Hello World!')
```

## Modifying Input and Output

* `<` changes the input
* `>` changes the output
* `2>` changes the error output
* `&>` changes the standard and the error output
* `>>` changes the output, but doesn't overwrite the file

```bash
user@ufpr:~/tests$ python script.py > output_file.txt
user@ufpr:~/tests$ cat output_file.txt
Hello World!
```
Copying using `cat`
```bash
user@ufpr:~/tests$ cat < outputfile.txt > copyfile.txt 
user@ufpr:~/tests$ cat copyfile.txt 
Hello World!
```

Changing the Error Output

```bash
user@ufpr:~/tests$ ls script{.py,.js}
ls: não foi possível acessar 'script.js': No such file or directory
script.py
user@ufpr:~/tests$ ls script{.py,.js} 2> error.txt
script.py
user@ufpr:~/tests$ cat error.txt 
ls: não foi possível acessar 'script.js': No such file or directory
```

# Pipeline

A pipe denoted by | is a way to link the output of a program to the input of another program

`wc` is command for Word Count, it shows how many lines, words and characters the file has

```bash
user@ufpr:~/tests$ cat hello.pas 
program hello;
begin
    writeln('Hello World!')
end.
user@ufpr:~/tests$ wc hello.pas
 4  6 54 hello.pas
```

`grep` filters a line of the file

```bash
user@ufpr:~/tests$ grep World hello.pas
    writeln('Hello World!')
```

If we want to know how many lines has the characters "ello"

```bash
user@ufpr:~/tests$ grep ello hello.pas
program hello;
    writeln('Hello World!')
user@ufpr:~/tests$ grep ello hello.pas | wc -l
2
```

Word Count of the **script.py** output
```bash
user@ufpr:~/tests$ python script.py | wc
      1       2      13
```
## Cut examples

There is a **data.csv** file in our directory, which contains:
| user | name | universisty |
| --- | ----------- | --- |
| vm20 | Vinicius | UFPR |
| axt19 | Aaron | USP |
js17 | John | UFSC |


Let's use **cut** to show a specific column (name)
```bash
user@ufpr:~/tests$ cut -d, -f2 data.csv 
 name
 Vinicius
 Aaron
 John
```

We can sort the output data
```bash
(base) vinicius@pop-os:~/tests$ cut -d, -f1 data.csv | sort 
axt19
js17
user
vm20
```