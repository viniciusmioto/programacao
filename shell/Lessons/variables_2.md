# Variables Part II

## Positional Parameters

The parameters are named with numbers: 1, 2, 3,... and they can be accessed by $1, $2, $3,...

The $0 content is the name of the script

Let's consider the following script

```bash
#!/bin/bash

echo $1
echo $2
echo $3
echo $4
echo $5
echo $6
echo $7
echo $8
echo $9
echo $10
```

If we execute it in the Shell like this:

```bash
tux@ufpr:~$ source my_script.sh pos1 pos2 pos3
```

we will have the following output:

```console
pos1
pos2
pos3






pos10
```

The first three parameters get the values that we pass in the command line "pos1", "pos2", "pos3" and they will be printed. But the last echo printed pos10, this happened because it get the $1 value, if we want to get the $10 value we need quoting ${10}

## Giving names to variables

It is good to give name to the variables, so we will know what that stand for

```bash
#!/bin/bash

DIR=$1
PERMISSION=$2

mkdir ${DIR}
chmod ${PERMISSION} ${DIR}
```

## Special positional variables

* @ and \* have all the positional variables, except $0
    * "$*" is a string with all the parameters
    * "$@" is a set of strings with the parameters "$1" "$2" "$N"
* $# has the number of parameters passed to the script

## Parameters in functions

It is exactly the same as the positional parameters for scripts

```bash
#!/bin/bash

message()
{
    echo $1
}

message 'hello'
message 'good bye'
```
```console
hello
good bye
```

## String Operators

```bash
#!/bin/bash

V="program.pas"
echo $V
echo ${V#program}
echo ${V%.pas}
```

```console
program.pas
.pas
program
```