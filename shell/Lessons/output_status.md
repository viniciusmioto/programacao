# Output Status

Every Shell command has a output status, but we can see it until we ask for it. In C language we use *return 0* or *return 1* in our programs, this is the output status.

The output is a value in [0, 255], usually 0 means that everything is ok and the other values means that something wrong happened

A special variable is `?` which has the value of the last output status

```bash
(base) tux@ufpr:~$ ls
 Documents   Models   Downloads ...
(base) tux@ufpr:~$ echo $?
0
(base) tux@ufpr:~$ ls /root
ls: could not open directory '/root': Permission denied
(base) tux@ufpr:~$ echo $?
2
tux@ufprs:~$ echo $?
0
```

The last command gives us 0 as the result because the command "echo $?" before it has gone ok